## Requirements

### python3

```
sudo apt-get install python3
```

### libtcod for python3

```
sudo pip3 install libtcod
```

## How to run the game

```
python3 engine.py
```
